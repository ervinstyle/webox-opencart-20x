<?php
$_['text_title']		='Nemzeti posta';
$_['text_weight']		='Súly:';
$_['text_insurance']		='Összeg biztosítva:';
$_['text_1st_class_standard']		='Elsőosztályú Standard küldés';
$_['text_1st_class_recorded']		='Elsőosztályú Tértivevényes küldés';
$_['text_2nd_class_standard']		='Másodosztályú Standard küldés';
$_['text_2nd_class_recorded']		='Másodosztályú Tértivevényes küldés';
$_['text_special_delivery_500']		='Speciális szállítás másnapra (500 font)';
$_['text_special_delivery_1000']		='Speciális szállítás másnapra (1000 font)';
$_['text_special_delivery_2500']		='Speciális szállítás másnapra (2500 font)';
$_['text_standard_parcels']		='Standard csomag';
$_['text_airmail']		='Légiposta';
$_['text_international_signed']		='Nemzetközileg aláírt';
$_['text_airsure']		='Légi biztosítva';
$_['text_surface']		='Felszíni';
?>