<?php
$_['heading_title']		='Szállítási díj kiszámítása';
$_['text_success']		='A szállítási díj kiszámításra került!';
$_['text_shipping']		='Adja meg a szállítási címet a kalkulációhoz.';
$_['text_shipping_method']		='Kérjük, válassza ki a kívánt szállítási módot ehhez a rendeléshez.';
$_['entry_country']		='Ország';
$_['entry_zone']		='Megye';
$_['entry_postcode']		='Irányítószám';
$_['error_postcode']		='Az irányítószám minimum 2 maximum 10 karakter lehet';
$_['error_country']		='Kérjük, válasszon egy országot!';
$_['error_zone']		='Kérjük, válasszon ki egy megyét!';
$_['error_shipping']		='Figyelem: szállítási mód megadása kötelező!';
$_['error_no_shipping']		='Figyelem: Nem található szállítási mód. Kérjük vegye fel a<a href="%s">kapcsolatot</a> ügyfélszolgálatunkkal!';
?>