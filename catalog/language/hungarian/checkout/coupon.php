<?php
$_['heading_title']		='Kuponkód';
$_['text_success']		='A kupon kedvezmény sikeresen jóváírva!';
$_['entry_coupon']		='Adja meg kupon kódját';
$_['error_coupon']		='A kupon érvénytelen, lejárt, vagy elérte a felhasználhatósági korlátot!';
$_['error_empty']		='Kérjük, írja be a kupon kódját!';
?>