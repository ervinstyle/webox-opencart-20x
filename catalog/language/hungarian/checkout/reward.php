<?php
$_['heading_title']		='Hűségpontok beváltása (%s)';
$_['text_success']		='A hűségpontok sikeresen levonva!';
$_['entry_reward']		='Hűségpont (Max %s)';
$_['error_reward']		='Kérjük, adja meg mennyi hűségpontot szeretne beváltani!';
$_['error_points']		='Figyelem: Ön nem rendelkezik %s hűségponttal!';
$_['error_maximum']		='Figyelem: A maximálisan felhasználható pontok száma: %s!';
?>