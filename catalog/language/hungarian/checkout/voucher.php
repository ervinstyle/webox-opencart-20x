<?php
$_['heading_title']		='Ajándékutalvány beváltása';
$_['text_success']		='Az ajándékutalvány sikeresen felhasználva!';
$_['entry_voucher']		='Adja meg az utalvány kódját';
$_['error_voucher']		='Figyelem: az ajándékutalvány kódja hibás vagy már felhasználásra került!';
$_['error_empty']		='Figyelmeztetés: Kérjük, adja meg az utalvány kódját!';
?>