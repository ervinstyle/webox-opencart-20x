<?php
$_['heading_title']		='Affiliate követés';
$_['text_account']		='Fiók';
$_['text_description']		='To make sure you get paid for referrals you send to us we need to track the referral by placing a tracking code in the URL\'s linking to us. You can use the tools below to generate links to the %s web site.';
$_['entry_code']		='Követőkód';
$_['entry_generator']		='Követőlink generálása';
$_['entry_link']		='Követőlink';
$_['help_generator']		='Írja be a termék nevét nevét amelyet szeretne követni';
?>