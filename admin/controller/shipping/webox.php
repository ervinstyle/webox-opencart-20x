<?php
class ControllerShippingWebox extends Controller
{
	private $error = array();
	public function index()
	{
		$this->language->load('shipping/webox');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('setting/setting');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate())
		{
			$this->model_setting_setting->editSetting('webox', $this->request->post);
			$this->session->data['success'] = $this->language->get('text_success');
			$this->response->redirect($this->url->link('extension/shipping', 'token=' . $this->session->data['token'], 'SSL'));
		}
		$data['text_edit'] = $this->language->get('text_edit');
		$data['heading_title']    = $this->language->get('heading_title');
		$data['text_enabled']     = $this->language->get('text_enabled');
		$data['text_disabled']    = $this->language->get('text_disabled');
		$data['text_all_zones']   = $this->language->get('text_all_zones');
		$data['text_none']        = $this->language->get('text_none');
		$data['entry_cost']       = $this->language->get('entry_cost');
		$data['entry_tax_class']  = $this->language->get('entry_tax_class');
		$data['entry_geo_zone']   = $this->language->get('entry_geo_zone');
		$data['entry_status']     = $this->language->get('entry_status');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['button_save']      = $this->language->get('button_save');
		$data['button_cancel']    = $this->language->get('button_cancel');
		if (isset($this->error['warning']))
		{
			$data['error_warning'] = $this->error['warning'];
		}
		else
		{
			$data['error_warning'] = '';
		}
		$data['breadcrumbs']   = array();
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);
		
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_shipping'),
			'href' => $this->url->link('extension/shipping', 'token=' . $this->session->data['token'], 'SSL')
		);
		
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('shipping/webox', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);
		$data['action']        = $this->url->link('shipping/webox', 'token=' . $this->session->data['token'], 'SSL');
		$data['cancel']        = $this->url->link('extension/shipping', 'token=' . $this->session->data['token'], 'SSL');
		$this->load->model('localisation/tax_class');
		$data['tax_classes'] = $this->model_localisation_tax_class->getTaxClasses();
		$this->load->model('localisation/geo_zone');
		$data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();
		if (isset($this->request->post['webox_status']))
		{
			$data['webox_status'] = $this->request->post['webox_status'];
		}
		else
		{
			$data['webox_status'] = $this->config->get('webox_status');
		}
		if (isset($this->request->post['webox_sort_order']))
		{
			$data['webox_sort_order'] = $this->request->post['webox_sort_order'];
		}
		else
		{
			$data['webox_sort_order'] = $this->config->get('webox_sort_order');
		}
		if (isset($this->request->post['webox_tax_class_id']))
		{
			$data['webox_tax_class_id'] = $this->request->post['webox_tax_class_id'];
		}
		else
		{
			$data['webox_tax_class_id'] = $this->config->get('webox_tax_class_id');
		}
		if (isset($this->request->post['webox_cost']))
		{
			$data['webox_cost'] = $this->request->post['webox_cost'];
		}
		else
		{
			$data['webox_cost'] = $this->config->get('webox_cost');
		}
		if (isset($this->request->post['webox_geo_zone_id']))
		{
			$data['webox_geo_zone_id'] = $this->request->post['webox_geo_zone_id'];
		}
		else
		{
			$data['webox_geo_zone_id'] = $this->config->get('webox_geo_zone_id');
		}
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('shipping/webox.tpl', $data));

	}
	protected function validate()
	{
		if (!$this->user->hasPermission('modify', 'shipping/webox'))
		{
			$this->error['warning'] = $this->language->get('error_permission');
		}
		$cost                              = str_replace(',', '.', $this->request->post['webox_cost']);
		$cost                              = preg_replace("/[^0-9.]/", "", $cost);
		$this->request->post['webox_cost'] = $cost;
		if (!$this->error)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
?>
