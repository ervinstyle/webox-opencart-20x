<?php
class ModelSaleWeboxOrder extends Model {

	public function getOrders($data = array()) {
		$sql = "SELECT o.order_id, CONCAT(o.lastname, ' ', o.firstname) AS customer, 
		o.email AS customer_email,
		o.telephone customer_phone,
		(SELECT os.name FROM " . DB_PREFIX . "order_status os WHERE os.order_status_id = o.order_status_id AND os.language_id = '" . (int)$this->config->get('config_language_id') . "') AS status, o.total, o.currency_code, o.currency_value, o.date_added, o.date_modified, o.shipping_address_1, o.webox_mashine FROM `" . DB_PREFIX . "order` o";

        $sql .= " WHERE o.shipping_code = 'webox.webox'";
				
		if (!empty($data['filter_order_status'])) {
			$sql .= " AND o.order_status_id = " . $this->db->escape($data['filter_order_status']) . "";
		}

		if (!empty($data['filter_order_id'])) {
			$sql .= " AND o.order_id = '" . (int)$data['filter_order_id'] . "'";
		}

		if (!empty($data['filter_date_added'])) {
			$sql .= " AND DATE(o.date_added) = DATE('" . $this->db->escape($data['filter_date_added']) . "')";
		}

		if (!empty($data['filter_date_modified'])) {
			$sql .= " AND DATE(o.date_modified) = DATE('" . $this->db->escape($data['filter_date_modified']) . "')";
		}

		$sort_data = array(
			'o.order_id',
			'customer',
			'o.date_added',
			'o.date_modified',
			'o.shipping_address_1'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY o.order_id";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

    public function getTotalOrders($data = array()) {
        $sql = "SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "order` o";

        $sql .= " WHERE o.shipping_code = 'webox.webox'";

        if (!empty($data['filter_order_status_id'])) {
            $sql .= " AND o.order_status_id LIKE '%" . $this->db->escape($data['filter_order_status_id']) . "%'";
        }

        if (!empty($data['filter_order_id'])) {
            $sql .= " AND o.order_id = '" . (int)$data['filter_order_id'] . "'";
        }

        if (!empty($data['filter_date_added'])) {
            $sql .= " AND DATE(o.date_added) = DATE('" . $this->db->escape($data['filter_date_added']) . "')";
        }

        if (!empty($data['filter_date_modified'])) {
            $sql .= " AND DATE(o.date_modified) = DATE('" . $this->db->escape($data['filter_date_modified']) . "')";
        }

        $query = $this->db->query($sql);

        return $query->row['total'];
    }
    
    public function phone_format($numbers)
	{
		
	$pn=preg_replace('/\D/',"",$numbers);
	if(substr($pn, 0, 1)=="0"){ $pn=substr($pn, 1); }
	if(substr($pn, 0, 1)=="6"){ $pn="3".$pn; }
	if(substr($pn, 0, 2)!="36"){ $pn="36".$pn; }
	
		$orszag = substr($pn,0,2);
		$pn = substr($pn,2);
		if(substr($pn,0,1)==1){
			//Budapest, egy számjegyű körzetszám
			$korzet = 1;
			$szam 	= substr($pn,1); 
			}else{
			$korzet = substr($pn,0,2); 
			$szam 	= substr($pn,2); 
			//két számjegyű körzetszám
				}

	return $korzet.''.$szam;
				 
	}
	
	public function getSizes()
	{
		$data['A'] = 'A (8x38x64 cm)';
		$data['B'] = 'B (19x38x64 cm)';
		$data['C'] = 'C (41x38x64 cm)';
		return $data;
	}
}
?>
